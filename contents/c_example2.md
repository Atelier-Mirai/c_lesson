# 解答例 ★★☆☆☆☆☆
<br>

1.毎日１％ずつこつこつ成長.  
  [include](star_2/one_percent.c)

2.「今、何時？」 と聞いて挨拶.  
[include](star_2/fixed_deposit.c)

3.かけ算九九の表.  
[include](star_2/multiplication_table.c)

4.計算ゲーム.  
[include](star_2/util.h)
[include](star_2/calculation_game1.c)

5.配列の１０個の数字の合計.  
[include](star_2/summation.c)

6.配列の１０個の数字 一番小さい数 二番目に小さい数.  
[include](star_2/minimum_number.c)

7.配列の１０個の数字 大きい順に並び替え.  
[include](star_2/selection_sort.c)

8.４６８０秒は、何時間何分何秒.  
[include](star_2/time.c)

9.干支を求める.  
[include](star_2/eto.c)

10.世界の人口.  
[include](star_2/population.c)

11.二の乗数.  
[include](star_2/power.c)

```
乗数  16進数        10進数
 1           2             2
 2           4             4
 3           8             8
 4          10            16
 5          20            32
 6          40            64
 7          80           128
 8         100           256
 9         200           512
10         400          1024
11         800          2048
12        1000          4096
13        2000          8192
14        4000         16384
15        8000         32768
16       10000         65536
20      100000       1048576
30    40000000    1073741824
40 10000000000 1099511627776
```
2の10乗は、1k(キロ) 約1000  
2の20乗は、1M(メガ) 約100万  
2の30乗は、1G(ギガ) 約10億  
2の40乗は、1T(テラ) 約1兆  

12.消費税.  
[include](star_2/consumption_tax.c)

13.福引き.  
[include](star_2/lottery.c)
