# 解答例 ★★★☆☆☆☆
<br>

1.1+2+3...  
[include](star_3/total_10000.c)

2.縦棒グラフ.  
[include](star_3/vertical_bar_graph.c)

3.昭和３０年生まれの方の干支.  
[include](star_3/eto2.c)

4.円の面積.  
[include](star_3/circle_area.c)

5.英単語の長さ.  
[include](star_3/word_length.c)

6.計算ゲーム.  
[include](star_3/calculation_game2.c)
(より均一な乱数が出るように改良)
[include](star_3/mt.h)
[include](star_3/calculation_game3.c)

8.BMI.   
[include](star_3/bmi.c)

9.閏年.  
[include](star_3/leap_year.c)

10.算用数字を漢数字に変換.  
[include](star_3/num2kan.c)
