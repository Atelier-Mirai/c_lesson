# コラム

* [コンピュータを創った人々](contents/scientist.md)
* [コンピュータの仕組み](contents/structure_of_the_computer.md)
* [単位のお話](contents/talk_unit.md)
* [画素の話](contents/picture.md)
* [タッチタイピング](contents/touch_typing.md)
* [ショートカットキー](contents/shortcut.md)
* [Unix コマンド](contents/unix_command.md)
