他のmdの読み込み
{% include "column.md" %}

![fit, right](../img/summary.png)

    ├── README.md
    ├── Contents
    │   ├── jquery.md
    │   └── ESLint.md
    ├── SUMMARY.md
    └── book.json

-   `SUMMARY.md` は目次ファイル
-   `book.json` はGitBookのメタファイル




# include
[include:-5](star_1/dice.c)

#import
[import:5-10, lang-c](star_1/dice.c)

# Example

## Usage

    npm install
    npm start
    open http://localhost:4000/

## import/include

[import, test.js](./lecture/hello.c)

## Hardcoded class


[import, test.ts, lang-typescript](lecture/hello.c)

### Sliced Code

[import:5-8](lecture/hello.c)

[import:5-8](lecture/hello.c)

### Snippet code

[import:marker0, lang-typescript](lecture/hello.c)

### Custom Title

[import:marker0, title:"Custom Title", lang-typescript](lecture/hello.c)
