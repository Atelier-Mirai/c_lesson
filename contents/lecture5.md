# ポインタ

## [C言語ポインタ完全制覇 (標準プログラマーズライブラリ) ](https://www.amazon.co.jp/C言語ポインタ完全制覇-標準プログラマーズライブラリ-前橋-和弥/dp/4774111422/ref=pd_sim_14_3?ie=UTF8&dpID=51FT9JJP2VL&dpSrc=sims&preST=_AC_UL160_SR126%2C160_&refRID=QDJ9NKT0MQR96CN9SNXH)
に詳細が記載されているので、読むと良いです。

## ポインタの基礎
[include](lecture_5/pointer.c)
[include](lecture_5/pointer2.c)
[include](lecture_5/pointer3.c)
[include](lecture_5/pointer4.c)
[include](lecture_5/pointer5.c)

## ポインタと関数
[include](lecture_5/p_and_f.c)
[include](lecture_5/p_and_f2.c)
[include](lecture_5/p_and_f3.c)

## ポインタのポインタ（ダブルポインタ）

[こちら](http://www9.plala.or.jp/sgwr-t/c/sec10-4.html)にも詳細な説明があります。

[include](lecture_5/double_pointer.c)
