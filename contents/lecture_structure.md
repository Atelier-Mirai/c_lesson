# 講座

## 構造体の基本

名前、年齢などのように異なるデータ型を、一緒に扱いたい場合があります。
文字「列」型や整数型の様に、「利用者」型があったら便利です。<br>
char型やint型は、C言語標準で用意されていた型ですが、プログラマが、新しい「型」を創造することも可能です。<br>
既存のデータ型を組み合わせて作られた新しい「型」のことを、「構造体」と呼びます。

[include](lecture_structure/structure.c)
[include](lecture_structure/structure2.c)
[include](lecture_structure/structure3.c)
[include](lecture_structure/structure4.c)

## 四角形型の構造体

iPhone では画面表示にrectangle型を用いたプログラミングが可能です。<br>
ここでは、そのイメージを示します。
[include](lecture_structure/structure_iphone_rectangle.c)

## 三角形の面積 (ヘロンの公式)
[include](lecture_structure/structure_triangle.c)
