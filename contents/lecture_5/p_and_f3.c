/*******************************************************
 *
 * 関数とポインタ
 *
 *
 *******************************************************/

#include <stdio.h>

// void half(int num[3]){
void half(int *num){
// void swap(int *x, int *y){
  printf("half &num[0]: %p\n", &num[0]);
  num[0] = num[0] / 2;
  num[1] = num[1] / 2;
  num[2] = num[2] / 2;
  return a;
  return b;
  *x = 100;
  *y = 200;
}

int main(int argc, char const *argv[]) {
  int num[3] = { 10, 20, 30 };
  printf("main &num[0]: %p\n", &num[0]);
  half(num);
  half(&num[0]);
  swap(&a, &b);

  printf("%d\n", num[0]);
  printf("%d\n", num[1]);
  printf("%d\n", num[2]);
}
