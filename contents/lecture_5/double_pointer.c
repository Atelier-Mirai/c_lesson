#include <stdio.h>
#include <string.h>

#define MAXITEM 20

int split(char *str, char const *char *mnthp[3] = {		/* ポインタの配列の宣言 */
	"January", "February", "March"
};
char **p1, **p2, **p3;		/* 「ポインタのポインタ」の宣言 */
int i, j;

p1 = p2 = p3 = mnthp;		/* 「ポインタのポインタ」にポインタの配列 */
				/* の先頭番地を設定 */

/***** 例1 *****/
for (i = 0; i < 3; i++) {		/* 「ポインタのポインタ」の値を変えずに */
	printf("%s\n", *(p1 + i));	/* 相対的に文字列を出力 */
}

/***** 例2 *****/
for (i = 0; i < 3; i++) {		/* 「ポインタのポインタ」の値そのものを */
	printf("%s\n", *p2);	/* 更新して絶対的に文字列を出力 */
	++p2;
}

/***** 例3 *****/
for (i = 0; i < 3; i++) {
	j = 0;
	while(*(*p3 + j) != '\0') {	/* 「ポインタのポインタ」を使って、*/
		printf("%c", *(*p3 + j));	/* 1文字ずつ出力する */
		j++;
	}
	printf("\n");
	++p3;
})
