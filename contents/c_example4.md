# 解答例 ★★★★☆☆☆
<br>

1.１０人の名前を点数順に並び替えて表示.  
[include](star_4/score_and_name_sort.c)

2.素数.  
[include](star_4/prime_number.c)

3.完全数.  
[include](star_4/perfect_number.c)

4.英単語帳.  
[include](star_4/word_book.c)

5.すごろく.  
[include](star_4/sugoroku.c)
