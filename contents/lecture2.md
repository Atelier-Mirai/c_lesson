# 繰り返し処理

## プログラムを構成する３つの要素  

* 逐次・順次
* 反復・繰り返し
* 判断・条件分岐


C言語には反復のために、３つの構文が用意されています。
* for文
* while文
* do-while文

while文が基本です。

for文      繰返し回数が分かっている時
while文    繰返し回数が不明の時 (前判定)
do-while文 　　　　　　　　　　 (後判定)

[include](lecture_2/loop.c)

## break / continue

[include](lecture_2/break_continue.c)
