# 条件分岐・配列



## 条件分岐  

条件分岐のための構文として、
* if 文
* switch 文
が用意されています。

[include](lecture_3/vegetable.c)

## 配列  

配列は、基本的なデータ構造です。

いくつもの変数を纏めて取り扱えると、繰り返し処理などで活用することが出来、とっても便利です。

* 一次元配列
[include](lecture_3/score1.c)
* 二次元配列
[include](lecture_3/score2.c)

## 文字列

C 言語では、文字列は、文字の配列として扱います。

[include](lecture_3/string.c)
