# 画素の話

### ドロー系とペイント系  

お絵かきソフトには、大きく分けて２種類あります。

* ドロー系
Affinity Designer, Illustrator etc
* ペイント系
Affinity Photo, Photoshop etc

点と線、つまりベクトルで描かれる画像データをベクタデータと呼
び、そうしたデータで絵を描くソフトをドローソフトと呼びます。
拡大しても画像が劣化しないという特徴があります。

これに対し、ドットで絵を描くツールをペイントソフトと呼びます。
（ベクタデータに対してラスタデータといいます。）
こちらは拡大するとジャギー(ギザギザ)が出てしまいます。
（ASCIIデジタル用語辞典より引用・改変）

### ベクタデータとラスタデータ  

ドロー系ソフトで、赤い丸を書いた場合は、データはベクタデータとして保持されます。つまり、画面の左から５ｃｍ、上から１０ｃｍの点を中心とした半径３ｃｍの円
、色は赤、といった具合に、ベクタデータとして保存されます。  
ですから、この丸を２倍に拡大しても、「半径６ｃｍの円を描く」ことになるだけで
すので、ギザギザになることはありません。

一方、ペイント系ソフトで、赤い丸を書いた場合には、データはラスタデータとして
保存されます。ちょうど、１ｍｍづつの方眼紙が縦に１００個、横に１００個集まって、一枚の絵が構成されていると想像してください。
そして、
左上から１番目、上から１番目の方眼は赤色、
左上から１番目、上から２番目の方眼は黄色、
左上から１番目、上から３番目の方眼は黄色、
・・・
左上から１００番目、上から１００番目の方眼は青色。
という具合です。

ですから、絵を２倍に拡大すると、今まで１ｍｍだった方眼が２ｍｍの方眼に変わる
わけですから、ギザギザが目立つことになります。

### 画素数、ppiについて

　画素とは、画像を構成する最小単位のことです。ピクセルとも言います。
先ほどの例では、１ｍｍの方眼が画素でしたが、実際の方眼のサイズは１ｍｍでありません。１インチ＝２．５４ｃｍの間にいくつ画素があるかを、ppi(pixel per inch)と言います。
　iPhone 6s Plus ですと、縦6.8インチ(122mm),横2.7インチ(68mm)の間に、1920*1080ピクセルありますから、1インチの間には、401個 画素があることになります。401ppiって目に見えないです。すごいですね。

 iPhone 6s / 6s Plus には１２００万画素(4032*3024)のデジカメが搭載されています。といったときには、一枚の写真が、１２００万個の画素で構成されていることを意味しています。つまり、横４０３２画素×横３０２４画素＝１２００万画素に一枚の写真を分割して、
左から１番目、上から１番目の方眼は赤色、
左から１番目、上から２番目の方眼は黄色、
左から１番目、上から３番目の方眼は黄色、
・・・
左から４０３２番目、上から３０２４番目の方眼は青色。
というようにして写真が保存されるということです。

### 写真のファイルサイズ

１２００万画素のデジカメは、横４０３２×縦３０２４＝１２００万画素でした。
そして、それぞれの画素には、赤、白、黄色など単純な色ばかりではなく、桜色や若
草色など微妙な色も含めて16777216色(=256*256*256)の色を持っています。

ですから、この写真を保存するために必要なファイルの大きさは、
4032*3024*3バイト（16777216色の情報を保存するためには、３バイトが必要なの
で）
＝36578304 バイト≒ 34.9メガバイトが必要になります。

### 色の表し方について

色の三原色は、赤、青、黄ですが、コンピュータでは、光の三原色、Red（赤）、Green（緑）、Blue（青）を用いて絵を表します。（RGBモデルといいます）
他に、色相（Hue）、彩度（Saturation）、明度（Blightness）によって色を表現
するHSBカラーモデルなどもあります。色彩画像関係にご興味のある方はどうぞ。
　html では色を表すのに、#ffffff（白色）といった表記ができます。いったい、この数字は何でしょう？また、先ほどのデジカメの写真で色情報を保存するのには３バイト必要といいましたが、どうしてなのでしょうか？

今、舞台を照らす３つのスポットライト、
Red（赤）、Green（緑）、Blue（青）があるとします。
Red（赤）が消えていて、Green(緑)が消えていて、Blue(青)が消えているとき、
舞台は黒色になります。（何も光がないのですから当然ですね）
Red（赤）が消えていて、Green(緑)が消えていて、Blue(青)がついているとき、
舞台は青色になります。（青がついているのですから当然ですね）
Red（赤）が消えていて、Green(緑)がついて、Blue(青)が消えているとき、
舞台は緑色になります。（緑がついているのですから当然ですね）
Red（赤）が消えていて、Green(緑)がついていて、Blue(青)がついているとき、
舞台は水色になります。
Red（赤）がついていて、Green(緑)が消えていて、Blue(青)が消えているとき、
舞台は赤色になります。
Red（赤）がついていて、Green(緑)が消えていて、Blue(青)がついているとき、
舞台は紫色になります。
Red（赤）がついていて、Green(緑)がついていて、Blue(青)が消えているとき、
舞台は黄色になります。
Red（赤）がついていて、Green(緑)がついていて、Blue(青)が消ついているとき、
舞台は白色になります。

　文章で書くと複雑ですが、
　それぞれのランプがついていることを、1
            ランプが消えていることを、0で表すことにすると、
 それぞれのランプの状態（ついているか消えているか）と、舞台の色との関係は次の表で表されます。

```
　それぞれのランプの色　　　　　　舞台の色　　ちなみに２進法と見なすと
　Ｒ（赤）　Ｇ（緑）　Ｂ（青）
　０　　　　０　　　　０　　　　　黒　　　　　０
　０　　　　０　　　　１　　　　　青　　　　　１
　０　　　　１　　　　０　　　　　緑　　　　　２
　０　　　　１　　　　１　　　　　水色　　　　３
　１　　　　０　　　　０　　　　　赤　　　　　４
　１　　　　０　　　　１　　　　　紫　　　　　５
　１　　　　１　　　　０　　　　　黄　　　　　６
　１　　　　１　　　　１　　　　　白　　　　　７
```

ここでは、消えている、ついている、という２つの状態を表すだけでしたら、０と１の２進法１桁だけでＯＫでしたが、実際には、とっても明るくついている時もあるでしょうし、ほとんど消えそうなくらいの明るさでついているときもあります。
そこで、明るさが変わるように、それぞれの電球は、２Ｖ（ボルト）の電池と、１Ｖ（ボルト）の電池につながっているとしましょう。
すると、２Ｖの電池につながっていなくて、１Ｖの電池にもつながっていないときには、電球の明るさは最低です。（電球がつかないのです（笑））
２Ｖの電池につながっていなくて、１Ｖの電池につながっているときには、電球の明るさはちょっと光ります。
２Ｖの電池につながっていて、１Ｖの電池にはつながっていないときには、電球の明るさはかなり光ります。
２Ｖの電池につながっていて、１Ｖの電池にもつながっているときには、電球の明るさは最高です。

　先ほどと同じように、電池のつなぎ具合を表にすると、

```
　２Ｖの電池　１Ｖの電池　電球の明るさ　　　　　　　　電球にかけられている電圧
      ０　　　　　０　　　レベル０（最低）             2V*0 + 1V*0 = 0V
      ０　　　　　１　　　レベル１（ちょっと明るい）   2V*0 + 1V*1 = 1V
      １　　　　　０　　　レベル２（かなり明るい）     2V*1 + 1V*0 = 2V
      １　　　　　１　　　レベル３（最高に明るい）　　 2V*1 + 1V*1 = 3V
```

　このように、２Ｖの電池と、１Ｖの電池を使うと、０Ｖ～３Ｖまでの４通りの明るさを表現することができました。
　同様に、４Ｖの電池と、２Ｖの電池と、１Ｖの電池を使うと、０Ｖ～７Ｖまでの８通りの明るさを表現することができます。
　同様に、１２８Ｖの電池と、６４Ｖの電池と、３２Ｖの電池と、１６Ｖの電池と、８Ｖの電池と、４Ｖの電池と、２Ｖの電池と、１Ｖの電池　合計８種類の電池を使うと、０Ｖ～２５５Ｖまでの２５６通りの明るさを表現することができます。

```
　１２８Ｖ　６４Ｖ　３２Ｖ　１６Ｖ　８Ｖ　４Ｖ　２Ｖ　１Ｖ  電圧
    ０　　　　０　　　０　　　０　　０　　０　　０　　０　　０Ｖ
    ・・・
    １　　　　１　　　１　　　１　　１　　１　　１　　１　　２５５Ｖ
```

　このようにそれぞれの桁の重みが２倍になっていくのが２進法です。
　（１０進法ではそれぞれの桁の重みは１０倍になります。１円玉より１０円玉は１０倍の重みがあります。１００円玉は１０円玉より１０倍の重みがあります。・・・）

　この２進法で、１桁のことを１ビット（bit)、８桁のことを１バイト（Byte）といいます。
　つまり１ビットでは、０と１の２通りの状態を表現できますし、
        １バイトでは、０から２５５までの２５６通りの状態を表現できます。
　
　もとのRed(赤),Green(緑),Blue(青)のランプの話に戻すと、それぞれのランプが
　全くついていない（０）～最高に明るく輝いている（２５５）までの段階を表すために１バイトずつ、計３バイト必要となります。
そして、赤、緑、青 それぞれで、256段階光の強さがありますから、全体では、3バイト 256*256*256 = 16777216色を表現することが出来ます。
16進数2桁で表現すると、#ffffff は、Rがff, Gがff, Bがffで、全部光っている状態のことですね。つまり、白です。

### クイズ  

７日分の給料の支払いとして金の延べ棒が１本ある。日払い希望のため、１日分ごとに線を引いた。６回はさみを入れて切り取れば、金の延べ棒は７等分されるので日払い可能である。しかし、金の延べ棒を６回も切り取るのは大変なため、切り取り回数は２回にしたい。どことどこを切り取ればよいか。

```
(金の延べ棒)
-------------------------------------------
|     |     |     |     |     |     |     |
-------------------------------------------
```





(答) １と２と４に分割すれば良いですね。

```
(金の延べ棒)
---------------------------------------------
|     ||     |     ||     |     |     |     |
---------------------------------------------
```

```
421
001 -> 1
010 -> 2
011 -> 3
100 -> 4
101 -> 5
110 -> 6
111 -> 7
```
