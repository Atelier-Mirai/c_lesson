## 難易度 ★★★☆☆☆☆
1. １＋２＋３＋・・・ と数を足していきましょう.    
どの数を足したときに, 合計が10000を越えるでしょうか？

1. 縦棒グラフを出してみましょう.
```
  □ □ ■ □ □ | (ヒント)
  □ ■ ■ □ □ | 画面に表示するときは,
  □ ■ ■ ■ □ | 左から右, 上から下が基本です.
  □ ■ ■ ■ ■ | 二次元配列を使って,
  ■ ■ ■ ■ ■ | 表示する順番を工夫しましょう.
```

1. 昭和３０年生まれの方の干支は何でしょうか?  
(S30 と入力します)

1. 円の面積を求めてみましょう.    
  円周率は, 3.14... と続きますが, よく使うので, Ｃ言語に用意されています.  
```c
  #include <math.h>  // 円周率 M_PI が定義されています
  #include <stdio.h>
  main(){
    printf("円周率 = %f",    M_PI);  // 特に書式を指定しなかった場合
    printf("円周率 = %.4f",  M_PI);  // 小数点以下４桁表示
    printf("円周率 = %.15f", M_PI);  // 小数点以下１５桁表示
  }
```

1. 英単語の長さを求めるソフトです.  
入力された英単語の長さを求めてみましょう.  
```
  I    -> 1文字.
  love -> 4文字.
  you  -> 3文字.
```

1. 計算ゲームです.  
２桁にしたり, 引き算や掛け算, 割り算もできる様にしてみましょう.  

1. 健康管理アプリです.  
朝食, 昼食, 夕食のカロリーの入力を求めてみましょう.    
  "食べ過ぎです"  
  "ちょうどです"  
 と表示しましょう.  

1. 太りすぎ, 痩せすぎの指標としてＢＭＩがあります.    
```
  体重[単位kg]/(身長[単位m]の二乗)
```
と計算します.    
身長と体重を入れると, 太りすぎか痩せすぎか分かるプログラムを作りましょう.    
(基準は22が標準体重, 25以上の場合を肥満, 18.5未満である場合, 低体重です)

1. 今年が平年か閏年かを求めるプログラムを作ってみましょう.    
4で割り切れる年は閏年です.    
但し100で割り切れる年は閏年ではありません.    
しかしながら, 400で割り切れる年は, 閏年です.  

1. 算用数字を漢数字にするプログラムを作ってみましょう.    
```
  例)302 -> 三百二
```

1. 成績管理システムを創ってみましょう.    
10人の生徒は国語, 算数, 理科, 社会を学んでいます.    
平均点, 最高点を求めてみましょう.  
