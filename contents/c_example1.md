# 解答例 ★☆☆☆☆☆☆
<br>

1.「おはようございます」と挨拶.  
  [include](star_1/goodmorning.c)

2.「今、何時？」 と聞いて挨拶.  
[include](star_1/greeting.c)

3.計算プログラム.  
[include](star_1/calculation.c)

4.A4用紙の面積.  
[include](star_1/a4_paper.c)

5.横棒グラフを表示.  
[include](star_1/bar_graph1.c)
[include](star_1/bar_graph2.c)
[include](star_1/bar_graph3.c)

6.さいころを転がし、「偶数」「奇数」表示.  
[include](star_1/dice.c)
(自作関数化した例)
[include](star_1/dice2.c)
(外部ヘッダーファイルにした例)
[include](star_1/random_number.h)
[include](star_1/dice3.c)

7.傘を持っていった方がよいか、アドバイス.  
[include](star_1/umbrella.c)
[include](star_1/umbrella2.c)
