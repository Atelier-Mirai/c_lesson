# 補足 その１

* 自作関数の例
[include](lecture_extra/add.c)

* 符号付き, 符号無し, 浮動小数点数の誤差
[include](lecture_extra/binary.c)

* ファイル一覧(Mac版)
[include](lecture_extra/file_entry.c)

* 文字化け対策の例
[include](lecture_extra/moji_shiftjis.c)

* メモリの動的確保
[include](lecture_extra/realloc_sample.c)

* 前置と後置
[include](lecture_extra/prefix_postfix.c)
