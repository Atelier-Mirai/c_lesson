#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[]) {

//  string subject_0 = "Japanease";
  char subject_0[] = "Japanease";

  char subject_1[] = "Mathematics";

//  string subject[] =  { "Japanease", "Mathematics", "Science" };
  char   subject[][20] = { "Japanease", "Mathematics", "Science" };
  int i;

  printf("%s\n", subject_0);
  printf("%s\n", subject_1);
  printf("---\n");
  printf("%s\n", subject[0]);
  printf("%s\n", subject[1]);
  printf("---\n");
  for (i = 0; i < 2; i++){
    printf("%s\n", subject[i]);
  }

  return 0;
}
