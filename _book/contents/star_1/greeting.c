#include <stdio.h>

int main(int argc, char const *argv[]) {

  // 変数の宣言
  int what_time_is_it_now;  // 今何時か、格納するための変数

  printf("今何時ですか？\n");             // 入力を促すために、メッセージを表示
  scanf("%d", &what_time_is_it_now);      // 整数型の数字を、受け取る

  if (what_time_is_it_now < 12) {         // 午前中
    printf("おはようございます。\n");     // 「\n」は「改行文字」で、改行されます。
  } else if (what_time_is_it_now < 18) {  // 夕方まで
    printf("こんにちは。\n");
  } else {                                // 夜なら
    puts("おやすみなさい。");             // puts関数を使うことも出来ます。
                                          // 改行まで行ってくれます。
  }
  return 0;
}
