#include <stdio.h>

int main(int argc, char const *argv[]) {

  int student[2][10] = { { 18, 25, 73, 30, 55, 18, 21, 89, 17, 31 },
                         { 28, 29, 33, 38, 15, 38, 51, 19, 77, 68 } };
                    //１０人の生徒の成績を格納する配列
  int top;          // 最高点
  int total;        // 合計点
  int average;      // 最低点
  int subject, i;

  // 平均点を求める処理
  for (subject = 0; subject < 2; subject++){
    total = 0;
    for(i = 0; i < 10; i++){
      // 合計点を求める
      total = total + student[subject][i];
    }
    average = total / 10;
    printf("平均点は %d です\n", average);
  }

  // 最高点を求める処理
  for (subject = 0; subject < 2; subject++){
    top = 0;
    for (i = 0; i < 10; i++){
      if (top < student[subject][i]){
        top = student[subject][i];
      }
    }
    printf("最高点は %d です\n", top);
  }

  return 0;
}
